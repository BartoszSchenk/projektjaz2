package com.example.jsfdemo.service;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import com.example.jsfdemo.domain.Anime;

@ApplicationScoped
public class AnimeManager {
	private List<Anime> db = new ArrayList<Anime>();

	public void addAnime(Anime anime) {
		Anime newAnime = new Anime();

		newAnime.setTitle(anime.getTitle());
		newAnime.setGenre(anime.getGenre());
		newAnime.setYear(anime.getYear());
		newAnime.setEpisodes(anime.getEpisodes());
		newAnime.setStudio(anime.getStudio());
		

		db.add(newAnime);
	}
	
	public void deleteAnime(Anime anime) {
		Anime animeToRemove = null;
		for (Anime a : db) {
			if (anime.getTitle().equals(a.getTitle())) {
				animeToRemove = a;
				break;
			}
		}
		if (animeToRemove != null)
			db.remove(animeToRemove);
	}

	public List<Anime> getAllAnimes() {
		return db;
	}
}
