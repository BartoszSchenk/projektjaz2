package com.example.jsfdemo.web;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIForm;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.model.ListDataModel;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;

import com.example.jsfdemo.domain.Anime;
import com.example.jsfdemo.service.AnimeManager;

@SessionScoped
@Named("animeBean")
public class AnimeFormBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Anime anime = new Anime();

	private ListDataModel<Anime> animes = new ListDataModel<Anime>();

	@Inject
	private AnimeManager an;

	public Anime getAnime() {
		return anime;
	}

	public void setAnime(Anime anime) {
		this.anime = anime;
	}

	public ListDataModel<Anime> getAllAnimes() {
		animes.setWrappedData(an.getAllAnimes());
		return animes;
	}

	
	public String addAnime() {
		an.addAnime(anime);
		return "showAnimes";
		
	}

	public String deleteAnime() {
		Anime animeToDelete = animes.getRowData();
		an.deleteAnime(animeToDelete);
		return null;
	}

}